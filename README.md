# Zhou_Chemrxiv_2022

Contains input file and data used to generate the figures of the article:

Effects of fluoride salt addition to the physico-chemical properties of the MgCl2-NaCl-KCl heat transfer fluid : a molecular dynamics study

Weiguang Zhou, Yanping Zhang, Mathieu Salanne

https://chemrxiv.org/engage/chemrxiv/article-details/61e562c37110e6e309673e17

The folder *input_files* contains a set of typical input files for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls)

The files *MgNaKCl.txt*, *MgNaKClF01.txt*, *MgNaKClF05.txt*, *MgNaKClF10.txt*, *MgNaKClF20.txt* contain the computed densities, viscosities and thermal conductivities at various temperatures for several compositions (provided in the header of the files)
